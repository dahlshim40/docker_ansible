FROM python:3.9
LABEL maintainer="Robin Lennox"

RUN apt update && \
    apt install -y --no-install-recommends vim nano dnsutils && \ 
    rm -rf /var/lib/apt/lists/* && \
    pip install ansible==5.8.0 ansible-lint requests yamllint && \
    pip cache purge && \
    groupadd -r ansible && \
    useradd --no-log-init -m -g ansible ansible && \
    mkdir /opt/ansible && \
    runuser -l ansible -c 'ansible-galaxy collection install scicore.guacamole vultr.cloud ngine_io.vultr'
COPY ansible.cfg /home/ansible/.ansible.cfg
RUN chown -R ansible:ansible /home/ansible/.ansible.cfg
WORKDIR /opt/ansible
COPY entrypoint.sh /home/ansible/entrypoint.sh
ENTRYPOINT ["/bin/sh", "/home/ansible/entrypoint.sh"]
CMD ["ansible-playbook"]

